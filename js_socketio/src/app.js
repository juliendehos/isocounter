"use strict";

const port = 3000;

const express = require("express");
const app = express();

const server = require("http").Server(app);
const io = require("socket.io")(server);

app.use("/", express.static("./static"));

let counter = 0;

app.get("/api", function (req, res) {
    const data = { counter };
    res.send(data);
});

io.on("connection", function(socket){
    socket.emit("update counter", counter);
    socket.on("increment counter", function(){
        counter += 1;
        io.emit("update counter", counter);
    });
});

server.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});

