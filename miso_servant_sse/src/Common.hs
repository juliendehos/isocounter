{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE TypeApplications           #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics (Generic)
import Miso
import Miso.String

newtype Model = Model 
    { counter_ :: Maybe Int
    } deriving (Eq, Generic, Show)

instance FromJSON Model
instance ToJSON Model

initialModel :: Model
initialModel = Model Nothing 

data Action
    = NoOp
    | SendIncrement Int
    | RecvCounter (Maybe Int)
    deriving (Show, Eq)

type ClientRoutes = HomeRoute

type HomeRoute = View Action

handlers :: Model -> View Action
handlers = homeRoute

homeRoute :: Model -> View Action
homeRoute (Model mc) = div_ 
    []
    [ h1_ [] [ text "isocounter (miso)" ]
    , div_
        []
        [ text counterStr
        , button_ [ onClick (SendIncrement 1) ] [ text "+" ]
        ]
    , p_ [] [ a_ [href_ "/api" ] [ text "api" ] ] 
    ]
    where counterStr = case mc of 
            Nothing -> "no counter"
            Just v -> toMisoString $ "counter: " ++ show v

