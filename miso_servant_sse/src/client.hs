{-# LANGUAGE OverloadedStrings          #-}

import Common
import Control.Monad (void)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String

main :: IO ()
main = miso $ \ _ -> App 
    { initialAction = SendIncrement 0
    , model = initialModel
    , update = updateModel
    , view = homeRoute
    , events = defaultEvents
    , subs = [ sseSub "/sse" recvSse ]
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel (SendIncrement inc) m = m <# (sendXhr inc >> pure NoOp)
updateModel (RecvCounter mc) m = noEff m { counter_ = mc }
updateModel NoOp m = noEff m

sendXhr :: Int -> IO ()
sendXhr c = void $ xhrByteString $ Request GET uri Nothing [] False NoData 
    where uri = toMisoString $ "increment/" ++ show c

recvSse :: SSE Int -> Action
recvSse (SSEMessage c) = RecvCounter (Just c)
recvSse _ = RecvCounter Nothing

