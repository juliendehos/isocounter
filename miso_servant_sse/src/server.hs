{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE TypeApplications           #-}

import           Common
import           Control.Concurrent (Chan, newChan, writeChan)
import           Control.Monad.IO.Class (liftIO)
import           Data.Binary.Builder (putStringUtf8)
import           Data.IORef (newIORef, readIORef, atomicModifyIORef, IORef)
import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Network.Wai (Application)
import           Network.Wai.EventSource (eventSourceAppChan, ServerEvent(..))
import           Network.Wai.Handler.Warp (run)
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           Servant

main :: IO ()
main = do
    counterRef <- newIORef (0 :: Int)
    chan <- newChan
    run 3000 $ logStdout (serverApp chan counterRef)

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.with 
                (L.script_ mempty) 
                [L.src_ "static/all.js", L.async_ mempty, L.defer_ mempty] 
        L.body_ (L.toHtml x)

type ServerRoutes = ToServerRoutes ClientRoutes HtmlPage Action

type ServerAPI
    =    "static" :> Raw 
    :<|> "sse" :> Raw 
    :<|> "increment" :> Capture "inc" Int :> Get '[JSON] NoContent 
    :<|> "api" :>  Get '[JSON] Model 
    :<|> ServerRoutes 

serverApp :: Chan ServerEvent -> IORef Int -> Application
serverApp chan counterRef = serve (Proxy @ServerAPI)
    (    serveDirectoryFileServer "static"
    :<|> Tagged (eventSourceAppChan chan)
    :<|> incrementHandler chan counterRef
    :<|> apiHandler counterRef
    :<|> serverHandlers 
    )

incrementHandler :: Chan ServerEvent -> IORef Int -> Int -> Handler NoContent
incrementHandler chan counterRef inc = do
    c' <- liftIO $ atomicModifyIORef counterRef (\ c -> let c' = c+inc in (c', c'))
    liftIO $ writeChan chan (ServerEvent Nothing Nothing [putStringUtf8 $ show c'])
    pure NoContent

apiHandler :: IORef Int -> Handler Model
apiHandler counterRef = liftIO (readIORef counterRef) >>= pure . Model . Just

serverHandlers :: Server ServerRoutes
serverHandlers = pure $ HtmlPage $ homeRoute initialModel

