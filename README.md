# isocounter

a simple example of isomorphic web app

![](isocounter.gif)


## initial setup

install nix


## js_*

```
make
```


## miso_*

```
make
```

or

```
nix-build
cd result
./bin/server
```

or

```
# in a first shell :
nix-shell -A client
make client

# in a second shell :
nix-shell -A server
make server
```


## wt

```
nix-shell --run "make run"
```

