#include "Api.hpp"
#include "App.hpp"

#include <Wt/WServer.h>

using namespace std;
using namespace Wt;

int main(int argc, char ** argv) {

    WServer server(argc, argv, WTHTTP_CONFIGURATION);

    Controller controller;

    auto mkApp = [&controller] (const WEnvironment & env) {
        return make_unique<App>(env, controller);
    };
    server.addEntryPoint(EntryPointType::Application, mkApp, "/");

    Api api(controller);
    server.addResource(&api, "/api");

    server.run();
    return 0;
}

