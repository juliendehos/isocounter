#include "Api.hpp"

#include <Wt/Json/Object.h>
#include <Wt/Json/Serializer.h>

using namespace std;
using namespace Wt;

Api::Api(const Controller & controller) : _controller(controller) {
}

Api::~Api() {
    beingDeleted();
}

void Api::handleRequest(const Http::Request &, Http::Response &resp) {
    Json::Object json;
    json["counter"] = _controller.getCounter();
    resp.setMimeType("application/json");
    resp.out() << Json::serialize(json);
}

