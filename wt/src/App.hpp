#ifndef APP_HPP
#define APP_HPP

#include <Wt/WApplication.h>
#include <Wt/WEnvironment.h>
#include <Wt/WText.h>

class Controller;

class App : public Wt::WApplication {
    private:
        Controller & _controller;
        Wt::WText * _uiCounter;

    public:
        App(const Wt::WEnvironment & env, Controller & controller);
        ~App();
        void updateCounter(int counter);
};

#include "Controller.hpp"

#endif

