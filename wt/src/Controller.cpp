#include "Controller.hpp"

#include <Wt/WApplication.h>
#include <Wt/WServer.h>

using namespace std;
using namespace Wt;

Controller::Controller() : _counter(0) {
}

void Controller::addClient(App * app) {
    unique_lock<mutex> lock(_mutex);
    _connections[app] = app->instance()->sessionId();
}

void Controller::removeClient(App * app) {
    unique_lock<mutex> lock(_mutex);
    _connections.erase(app);
}

int Controller::getCounter() const {
    unique_lock<mutex> lock(_mutex);
    return _counter;
}

void Controller::incrementCounter() {
    unique_lock<mutex> lock(_mutex);
    _counter++;
    for (auto & conn : _connections) {
        auto updateFunc = bind(&App::updateCounter, conn.first, _counter);
        WServer::instance()->post(conn.second, updateFunc);
    }
}

