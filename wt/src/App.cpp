#include "App.hpp"

#include <Wt/WContainerWidget.h>
#include <Wt/WPushButton.h>
#include <Wt/WTemplate.h>

using namespace std;
using namespace Wt;

const std::string APP_TEMPLATE = R"(
    <h1>isocounter (wt)</h1>
    <p>counter: ${counter} ${button}</p>
    <p><a href="/api">api</a></p>
)";

App::App(const WEnvironment & env, Controller & controller) :
    WApplication(env), _controller(controller)
{
    auto counterStr = to_string(_controller.getCounter());
    auto tmpl = root()->addWidget(std::make_unique<Wt::WTemplate>(APP_TEMPLATE));
    _uiCounter = tmpl->bindWidget("counter", std::make_unique<Wt::WText>(counterStr));
    auto button = tmpl->bindWidget("button", std::make_unique<Wt::WPushButton>("+"));

    _controller.addClient(this);
    enableUpdates(true);

    button->clicked().connect([this](){_controller.incrementCounter();});
}

App::~App() {
    _controller.removeClient(this);
    enableUpdates(false);
}

void App::updateCounter(int counter) {
    _uiCounter->setText(to_string(counter));
    triggerUpdate();
}

