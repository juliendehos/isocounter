#ifndef API_HPP
#define API_HPP

#include <Wt/Http/Request.h>
#include <Wt/Http/Response.h>
#include <Wt/WResource.h>

class Controller;

class Api : public Wt::WResource {
    private:
        const Controller & _controller;

    public:
        Api(const Controller & controller);
        ~Api();
        void handleRequest(const Wt::Http::Request &, Wt::Http::Response &) override;
};

#include "Controller.hpp"

#endif

