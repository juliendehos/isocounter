#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <map>
#include <mutex>
#include <vector>

#include <Wt/WPainter.h>

class App;

class Controller {
    private:
        mutable std::mutex _mutex;
        std::map<App*, std::string> _connections;
        int _counter;

    public:
        Controller();
        void addClient(App * app);
        void removeClient(App * app);
        int getCounter() const;
        void incrementCounter();
};

#include "App.hpp"

#endif

