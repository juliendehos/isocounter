{ stdenv, fetchFromGitHub, cmake, boost, openssl, zlib }:

stdenv.mkDerivation {

  name = "wt";

  src = fetchFromGitHub {
    owner = "emweb";
    repo = "wt";
    rev = "4.0.5";
    sha256 = "1gn8f30mjmn9aaxdazk49wijz37nglfww15ydrjiyhl6v5xhsjdv";
  };

  enableParallelBuilding = true;

  buildInputs = [ cmake boost openssl zlib ];

  cmakeFlags = [ "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_TESTS=OFF" "-DBUILD_EXAMPLES=OFF" ];
}
