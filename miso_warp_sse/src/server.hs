{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (Chan, newChan, writeChan)
import Data.Aeson (encode)
import Data.Text (unpack)
import Data.IORef (newIORef, readIORef, atomicModifyIORef, IORef)
import Common
import Data.Binary.Builder (putStringUtf8)
import Lucid
import Network.HTTP.Types (status200)
import Network.Wai (Application, pathInfo, responseFile, responseLBS)
import Network.Wai.EventSource (ServerEvent(..), eventSourceAppChan)
import Network.Wai.Handler.Warp (run)

type State = IORef Counter

homePage :: Html ()
homePage = doctypehtml_ $ do
  head_ $ do
    meta_ [charset_ "utf-8"]
    with (script_ mempty) [ src_ "all.js" ]
  body_ mempty

app :: State -> Chan ServerEvent -> Application
app state chan req respond = case pathInfo req of
    [] -> respond $ responseLBS status200 [("Content-Type", "text/html")] (renderBS homePage) 
    ["all.js"] -> respond $ responseFile status200 [("Content-Type", "application/javascript")] "static/all.js" Nothing 
    ["api"] -> do
        counter <- readIORef state
        respond $ responseLBS status200 [("Content-Type", "application/json")] (encode counter)
    ["sse"] -> eventSourceAppChan chan req respond 
    ["increment", incStr] -> do
        let inc = read $ unpack incStr
        v <- atomicModifyIORef state $ \ (Counter v) -> (Counter (v+inc), v+inc)
        writeChan chan (ServerEvent Nothing Nothing [putStringUtf8 $ show v])
        respond $ responseLBS status200 mempty mempty
    p  -> error $ "unexpected pathInfo: " ++ show p

main :: IO ()
main = do
    state <- newIORef $ Counter 0
    chan <- newChan
    run 3000 (app state chan)

