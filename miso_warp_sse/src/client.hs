{-# LANGUAGE OverloadedStrings #-}

import Common
import Control.Monad (void)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String

type Model = Counter

data Action
  = IncrementCounter Int
  | UpdateCounter Int
  | NoOp
  deriving (Show, Eq)

main :: IO ()
main = startApp App 
    { initialAction = IncrementCounter 0
    , model      = Counter (-1)
    , update     = updateModel
    , view       = viewModel
    , events     = defaultEvents
    , subs       = [ sseSub "sse" recvUpdateSse ]
    , mountPoint = Nothing
    }

recvUpdateSse :: SSE Int -> Action
recvUpdateSse (SSEMessage v) = UpdateCounter v
recvUpdateSse _ = NoOp

sendIncXhr :: Int -> IO ()
sendIncXhr v = void $ xhrByteString $ Request GET uri Nothing [] False NoData 
    where uri = toMisoString $ "increment/" ++ show v

updateModel :: Action -> Model -> Effect Action Model
updateModel (IncrementCounter v) m = m <# (sendIncXhr v >> pure NoOp)
updateModel (UpdateCounter v) m = noEff m { counter_ = v }
updateModel NoOp m = noEff m

viewModel :: Model -> View Action
viewModel m = div_ [] 
    [ h1_ [] [ text "isocounter (miso_warp_sse)" ]
    , p_ 
        []
        [ text "counter: "
        , text (toMisoString $ counter_ m)
        , button_ [ onClick (IncrementCounter 1) ] [ text "+" ]
        ]
    , p_ [] [ a_ [href_ "/api" ] [ text "api" ] ] 
    ]

