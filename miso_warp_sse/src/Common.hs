{-# LANGUAGE DeriveGeneric #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics (Generic)

newtype Counter = Counter 
    { counter_ :: Int
    } deriving (Eq, Generic, Show)

instance FromJSON Counter
instance ToJSON Counter

