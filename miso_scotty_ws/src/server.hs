{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent (forkIO)
import Control.Exception (finally)
import Control.Monad (forever)
import Common (Counter(..))
import Data.Aeson.Text
import Data.IORef
import Lucid
import Network.Wai.Middleware.Static (static)
import Network.WebSockets
import Web.Scotty

type State = IORef (Counter, Int, [(Int, Connection)])

wsServer :: State -> PendingConnection -> IO ()
wsServer state pc = do
    conn <- acceptRequest pc
    forkPingThread conn 30
    let addConn (c, n, cs) = ((c, n+1, (n, conn):cs), (c, n))
    (c, n) <- atomicModifyIORef state addConn
    sendTextData conn $ encodeToLazyText c
    finally (wsHandler conn state) (wsDisconnect n state)

wsHandler :: Connection -> State -> IO ()
wsHandler conn state = forever $ do
    _ <- receiveDataMessage conn
    let incCounter (Counter cv, n, cs) = ((c', n, cs), (c', cs))
            where c' = Counter (cv+1)
    (c, cs) <- atomicModifyIORef state incCounter
    mapM_ (\ (_, conn') -> sendTextData conn' $ encodeToLazyText c) cs 

wsDisconnect :: Int -> State -> IO ()
wsDisconnect ni state = 
    modifyIORef state (\ (c, n, cs) -> (c, n, filter ((/= ni).fst) cs))

homePage :: Html ()
homePage = html_ $ head_ $ script_ [src_ "/static/all.js"] ("" :: String)

scottyServer :: State -> ScottyM ()
scottyServer state = do
    get "/api" $ do
        (c, _, _) <- liftAndCatchIO $ readIORef state
        json c
    get "/" $ html $ renderText homePage
    middleware static

main :: IO ()
main = do
    state <- newIORef (Counter 0, 0, [])
    _ <- forkIO $ runServer "0.0.0.0" 3001 (wsServer state)
    scotty 3000 (scottyServer state)

