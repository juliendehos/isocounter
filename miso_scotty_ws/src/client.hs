{-# LANGUAGE OverloadedStrings #-}

import Common
import Miso
import Miso.String

type Model = Counter

data Action
  = IncrementCounter
  | UpdateCounter (WebSocket Counter)
  | NoOp
  deriving (Show, Eq)

main :: IO ()
main = startApp App 
    { initialAction = NoOp
    , model      = Counter (-1)
    , update     = updateModel
    , view       = viewModel
    , events     = defaultEvents
    , subs       = [ websocketSub url protocols UpdateCounter ]
    , mountPoint = Nothing
    }
    where url = URL "ws://localhost:3001"
          protocols = Protocols []

updateModel :: Action -> Model -> Effect Action Model
updateModel IncrementCounter m = m <# do send m >> pure NoOp
updateModel (UpdateCounter (WebSocketMessage c)) _ = noEff c
updateModel _ m = noEff m

viewModel :: Model -> View Action
viewModel m = div_ [] 
    [ h1_ [] [ text "isocounter (miso_scotty_ws)" ]
    , p_ []
        [ text "counter: "
        , text (toMisoString $ counter m)
        , button_ [ onClick IncrementCounter ] [ text "+" ]
        ]
    , p_ [] [ a_ [href_ "/api" ] [ text "api" ] ] 
    ]

