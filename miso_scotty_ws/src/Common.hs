{-# LANGUAGE DeriveGeneric #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics (Generic)

data Counter = Counter 
    { counter :: Int
    } deriving (Eq, Generic, Show)

instance FromJSON Counter
instance ToJSON Counter

