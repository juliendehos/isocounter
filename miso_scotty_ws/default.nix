let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "isocounter" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "isocounter" ./. {};

in

  pkgs.runCommand "isocounter" { inherit client server; } ''
    mkdir -p $out/{bin,static}
    cp ${server}/bin/* $out/bin/
    cp ${client}/bin/client.jsexe/all.js $out/static/
  ''

