let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "isocounter" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "isocounter" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

