{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Common
import Data.Proxy 
import Data.Text (Text)
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.API
import Servant.Client
import Servant

type ClientApi = NameApi :<|> PersonApi :<|> RepApi

name :<|> person :<|> rep = client (Proxy :: Proxy ClientApi)

queries1 :: ClientM (Text, Person, [Double])
queries1 = do
    n <- name 1
    p <- person
    l <- rep 2 4
    return (n, p, l)

queries2 :: ClientM (Text, [Double])
queries2 = (,) <$> name 2 <*> rep 2 4

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    let env = mkClientEnv manager (BaseUrl Http "localhost" 3000 "")
    runClientM queries1 env >>= print
    runClientM queries2 env >>= print

