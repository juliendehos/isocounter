{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant.API

data Person = Person 
    { name_ :: Text
    , age_  :: Int
    } deriving (Eq, Generic, Show)

instance FromJSON Person
instance ToJSON Person

type PersonApi = "person" :> Get '[JSON] Person

type NameApi = "name" :> Capture "id" Int :> Get '[PlainText] Text

type RepApi = "rep" :> Capture "nb" Int :> Capture "float" Double :> Get '[JSON] [Double]

