{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import Data.Text (Text)
import Network.Wai.Handler.Warp (run)
import Servant

main :: IO ()
main = run 3000 $ serve (Proxy @ServerApi) serverApi

type ServerApi 
    =    "static" :> Raw
    :<|> "42" :> Get '[JSON] Int
    :<|> NameApi
    :<|> PersonApi
    :<|> RepApi

serverApi :: Server ServerApi
serverApi 
    =    serveDirectoryFileServer "static"
    :<|> return 42
    :<|> return . nameById
    :<|> return (Person "Toto" 42)
    :<|> (\ n f -> return $ replicate n f)

nameById :: Int -> Text
nameById 1 = "Isaac Newton"
nameById 2 = "Marie Curie"
nameById _ = "unknown"

