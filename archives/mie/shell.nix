let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "mie" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "mie" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

