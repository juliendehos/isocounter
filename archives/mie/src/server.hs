{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import qualified Common
import           Data.Proxy (Proxy(..))
import qualified Lucid                                as L
import qualified Miso
import qualified Network.HTTP.Types                   as HTTP
import qualified Network.Wai                          as Wai
import qualified Network.Wai.Handler.Warp             as Wai
import qualified Network.Wai.Middleware.Gzip          as Wai
import qualified Network.Wai.Middleware.RequestLogger as Wai
import qualified Servant
import           Servant ((:>), (:<|>)(..))

main :: IO ()
main = Wai.run 3000 $ Wai.logStdout $ compress app
    where compress = Wai.gzip Wai.def { Wai.gzipFiles = Wai.GzipCompress }

type ServerAPI
    =    ("static" :> Servant.Raw)
    :<|> (Miso.ToServerRoutes Common.ViewRoutes HtmlPage Common.Action)
    :<|> Servant.Raw

app :: Wai.Application
app = Servant.serve (Proxy @ServerAPI)
    (    Servant.serveDirectoryFileServer "static"
    :<|> (homeServer :<|> flippedServer)
    :<|> Servant.Tagged page404
    )

homeServer :: Servant.Server (Miso.ToServerRoutes Common.Home HtmlPage Common.Action)
homeServer = pure $ HtmlPage $ Common.viewModel $ Common.initialModel Common.homeLink

flippedServer :: Servant.Server (Miso.ToServerRoutes Common.Flipped HtmlPage Common.Action)
flippedServer = pure $ HtmlPage $ Common.viewModel $ Common.initialModel Common.flippedLink

page404 :: Wai.Application
page404 _ respond = 
    respond $ 
    Wai.responseLBS HTTP.status404 [("Content-Type", "text/html")] $
    L.renderBS $ 
    L.toHtml Common.page404View

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = do
        L.doctype_
        L.head_ $ do
            L.title_ "Miso isomorphic example"
            L.meta_ [L.charset_ "utf-8"]
            L.with 
                (L.script_ mempty)
                [L.src_ "/static/all.js", L.async_ mempty, L.defer_ mempty]
        L.body_ (L.toHtml x)

