{-# LANGUAGE TypeOperators #-}

import Common
import Miso

main :: IO ()
main = miso $ \currentURI -> App
    { initialAction = NoOp
    , model         = initialModel currentURI
    , update        = updateModel
    , view          = viewModel
    , events        = defaultEvents
    , subs          = [uriSub HandleURIChange]
    , mountPoint    = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel AddOne m = noEff m { _counterValue = _counterValue m + 1 }
updateModel SubtractOne m = noEff m { _counterValue = _counterValue m - 1 }
updateModel (HandleURIChange uri) m = noEff m { _uri = uri }
updateModel (ChangeURI uri) m = m <# (pushURI uri >> pure NoOp)

