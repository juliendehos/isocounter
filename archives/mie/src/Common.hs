{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Common where

import Data.Proxy (Proxy(..))
import Servant.API ((:<|>)(..), (:>))
import Servant.Links (linkURI, safeLink)
import Miso
import Miso.String (ms)
import Network.URI (URI(..))

data Model = Model
    { _uri          :: URI
    , _counterValue :: Int
    } deriving (Eq, Show)

initialModel :: URI -> Model
initialModel uri = Model
    { _uri = uri
    , _counterValue = 0
    }

data Action
    = NoOp
    | AddOne
    | SubtractOne
    | ChangeURI URI
    | HandleURIChange URI
    deriving (Show, Eq)

type Home = View Action

type Flipped = "flipped" :> View Action

type ViewRoutes = Home :<|> Flipped

viewRoutes :: (Model -> View Action) :<|> (Model -> View Action)
viewRoutes = homeView :<|> flippedView

-- Checks which URI is open and shows the appropriate view
viewModel :: Model -> View Action
viewModel m = case runRoute (Proxy @ViewRoutes) viewRoutes _uri m of
    Left _routingError -> page404View
    Right v -> v

homeView :: Model -> View Action
homeView m =
    div_ []
         [ div_
             []
             [ button_ [ onClick SubtractOne ] [ text "-" ]
             , text $ ms $ show $ _counterValue m
             , button_ [ onClick AddOne ] [ text "+" ]
             ]
         , button_ [ onClick $ ChangeURI flippedLink ] [ text "Go to /flipped" ]
         ]

flippedView :: Model -> View Action
flippedView m =
    div_ []
         [ button_ [ onClick $ ChangeURI homeLink ] [ text "Go to /" ]
         , div_
             []
             [ button_ [ onClick AddOne ] [ text "+" ]
             , text $ ms $ show $ _counterValue m
             , button_ [ onClick SubtractOne ] [ text "-" ]
             ]
         ]

page404View :: View Action
page404View = text "Yo, 404, page unknown. Go to / or /flipped. Shoo!"

homeLink :: URI
homeLink = linkURI $ safeLink (Proxy @ViewRoutes) (Proxy @Home)

flippedLink :: URI
flippedLink = linkURI $ safeLink (Proxy @ViewRoutes) (Proxy @Flipped)

