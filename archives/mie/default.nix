let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "mie" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "mie" ./. {};

in

  pkgs.runCommand "mie" { inherit client server; } ''
    mkdir -p $out/{bin,static}
    cp ${server}/bin/* $out/bin/
    cp ${client}/bin/client.jsexe/all.js $out/static/
  ''

