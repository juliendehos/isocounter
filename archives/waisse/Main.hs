{-# LANGUAGE OverloadedStrings #-}

import Data.ByteString.Builder (string8)
import Control.Concurrent (forkIO, threadDelay)
import Control.Concurrent.Chan
import Control.Monad (forever)
import Data.Time.Clock.POSIX (getPOSIXTime)
import Network.HTTP.Types (status200)
import Network.Wai (Application, pathInfo, responseFile)
import Network.Wai.EventSource (ServerEvent(..), eventSourceAppChan)
import Network.Wai.Handler.Warp (run)

app :: Chan ServerEvent -> Application
app chan req respond = case pathInfo req of
    []      -> respond $ responseFile status200 [("Content-Type", "text/html")] "index.html" Nothing
    ["sse"] -> eventSourceAppChan chan req respond
    _       -> error "unexpected pathInfo"

eventChan :: Chan ServerEvent -> IO ()
eventChan chan = forever $ do
    threadDelay 1000000
    time <- getPOSIXTime
    writeChan chan (ServerEvent Nothing Nothing [string8 . show $ time])

main :: IO ()
main = do
    chan <- newChan
    _ <- forkIO . eventChan $ chan
    run 3000 (app chan)

