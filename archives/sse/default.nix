let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "sse" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "sse" ./. {};

in

  pkgs.runCommand "sse" { inherit client server; } ''
    mkdir -p $out/{bin,static}
    cp ${server}/bin/* $out/bin/
    cp ${client}/bin/client.jsexe/all.js $out/static/
  ''

