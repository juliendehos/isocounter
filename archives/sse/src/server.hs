{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE TypeApplications           #-}

import           Common
import           Control.Concurrent (Chan, forkIO, newChan, writeChan, threadDelay)
import           Control.Monad (forever)
import           Data.Binary.Builder (putStringUtf8)
import           Data.Proxy (Proxy(..))
import           Data.Time.Clock (getCurrentTime)
import qualified Lucid as L
import           Lucid.Base (renderBS, toHtml)
import           Miso
import           Network.HTTP.Types (status404)
import           Network.Wai (Application, responseLBS)
import           Network.Wai.EventSource (eventSourceAppChan, ServerEvent(..))
import           Network.Wai.Handler.Warp (run)
import           Network.Wai.Middleware.RequestLogger (logStdout)
import           Servant

main :: IO ()
main = do
  chan <- newChan
  _ <- forkIO (sendEvents chan)
  run 3000 $ logStdout (app chan)

sendEvents :: Chan ServerEvent -> IO ()
sendEvents chan = forever $ do
    time <- getCurrentTime
    writeChan
        chan
        (ServerEvent Nothing Nothing [putStringUtf8 (show (show time))])
    threadDelay (1000000 :: Int)

newtype HtmlPage a = HtmlPage a
  deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
  toHtmlRaw = L.toHtml
  toHtml (HtmlPage x) =
    L.doctypehtml_ $ do
      L.head_ $ do
        L.meta_ [L.charset_ "utf-8"]
        L.with 
            (L.script_ mempty) 
            [L.src_ "/static/all.js", L.async_ mempty, L.defer_ mempty] 
      L.body_ (L.toHtml x)

type ServerRoutes = ToServerRoutes ClientRoutes HtmlPage Action

type API 
    =    "static" :> Raw 
    :<|> "sse" :> Raw 
    :<|> ServerRoutes 
    :<|> Raw

app :: Chan ServerEvent -> Application
app chan = serve (Proxy @API)
    (    serveDirectoryFileServer "static"
    :<|> Tagged (eventSourceAppChan chan)
    :<|> serverHandlers 
    :<|> Tagged handle404
    )

serverHandlers :: Server ServerRoutes
serverHandlers = pure $ HtmlPage $ home $ Model homeLink "No event received"

handle404 :: Application
handle404 _ respond =
  respond $
  responseLBS status404 [("Content-Type", "text/html")] $
  renderBS $
  toHtml $
  HtmlPage the404

