{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}

import Common
import Data.Proxy
import Miso

main :: IO ()
main = miso $ \currentURI -> App 
    { initialAction = NoOp
    , model = Model currentURI "No event received"
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = [ sseSub "/sse" handleSseMsg, uriSub HandleURI ]
    , mountPoint = Nothing
    }

viewModel :: Model -> View Action
viewModel m = case runRoute (Proxy @ClientRoutes) handlers modelUri m of
    Left _ -> the404
    Right v -> v

handleSseMsg :: SSE String -> Action
handleSseMsg (SSEMessage msg) = ServerMsg msg
handleSseMsg SSEClose = ServerMsg "SSE connection closed"
handleSseMsg SSEError = ServerMsg "SSE error"

updateModel :: Action -> Model -> Effect Action Model
updateModel (ServerMsg msg) m = pure (m {modelMsg = "Event received: " ++ msg})
updateModel (HandleURI u) m = m {modelUri = u} <# pure NoOp
updateModel (ChangeURI u) m = m <# (pushURI u >> pure NoOp)
updateModel NoOp m = noEff m

