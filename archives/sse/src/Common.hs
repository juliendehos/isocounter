{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}

module Common where

import Data.Proxy
import Miso
import Miso.String
import Servant.API
import Servant.Links

data Model = Model 
    { modelUri :: URI
    , modelMsg :: String
    } deriving (Show, Eq)

data Action
  = ServerMsg String
  | NoOp
  | ChangeURI URI
  | HandleURI URI
  deriving (Show, Eq)

-- There is only a single route in this example
type ClientRoutes = Home

type Home = View Action

handlers :: Model -> View Action
handlers = home

home :: Model -> View Action
home (Model _ msg) = div_ [] [div_ [] [h3_ [] [text "SSE Example"]], text $ ms msg]

homeLink :: URI
homeLink = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @Home)

the404 :: View Action
the404 = div_ [] 
    [ text "404: Page not found "
    , button_ [onClick $ ChangeURI homeLink] [text "Go Home"]
    ]

