let

  pkgs = import ./nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "sse" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "sse" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

